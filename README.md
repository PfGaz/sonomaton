# Le Sonomaton

Le projet sonomaton a été développé au sein de l'atelier partagé. Il s'agit
d'un photomaton dans lequel, au lieu d'enregistrer une photo, on enregistre un
son. A la sortie, au lieu d'obtenir une photo, on obtient des QR code autocollants
qui renvoient vers le son en ligne.
